/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lam.tran.vn.dailytaskautomator.R;
import lam.tran.vn.dailytaskautomator.adapters.ProfileCursorAdapter;
import lam.tran.vn.dailytaskautomator.interfaces.IFragmentSwap;
import lam.tran.vn.dailytaskautomator.models.ProfileDetailObject;
import lam.tran.vn.dailytaskautomator.models.ProfileObject;
import lam.tran.vn.dailytaskautomator.providers.ProfilesProvider;
import lam.tran.vn.dailytaskautomator.utils.DbUtils;
import lam.tran.vn.dailytaskautomator.utils.FunctionalUtils;

/**
 * @author Lam Tran on 3/1/14.
 *         <p/>
 *         This is the main fragment that show the list of created profiles.
 */
public class ProfileFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ProfileCursorAdapter.IFinishLoad, ProfileCursorAdapter.IProfileCheckChanged {
    private IFragmentSwap mActivity;
    private ListView mListView;
    private TextView mTextView;
    private LinearLayout mButtonLayout;
    private Set<ProfileObject> mSetProfiles;
    private boolean mShowDeleteMenu;
    private Set<Long> mDeleteIds;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof IFragmentSwap)) {
            throw new ClassCastException("Not IFragmentSwap");
        }
        mActivity = (IFragmentSwap) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        if (rootView == null || getActivity() == null) {
            return null;
        }

        mButtonLayout = (LinearLayout) rootView.findViewById(R.id.btn_layout);
        mTextView = (TextView) rootView.findViewById(R.id.profile_textview);
        mListView = (ListView) rootView.findViewById(R.id.profile_listview);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mActivity.setCurrentIndex(Integer.parseInt(view.getTag(R.string.profile_index).toString()));
                mActivity.setProfileName(view.getTag(R.string.profile_name).toString());
                mActivity.onFragmentPush(EditProfileFragment.class);
            }
        });

        getActivity().getLoaderManager().initLoader(1, null, this);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profiles, menu);
        menu.findItem(R.id.trash_menu_item).setVisible(mShowDeleteMenu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_create_profile:
                mActivity.setCurrentIndex(-1);
                mActivity.setProfileName(null);
                mActivity.onFragmentPush(EditProfileFragment.class);
                break;
            case R.id.trash_menu_item:
                DbUtils.removeProfile(getActivity(), mDeleteIds);
                ((ProfileCursorAdapter) mListView.getAdapter()).resetRemoveChecked();
                mShowDeleteMenu = false;
                if (getActivity() != null) getActivity().invalidateOptionsMenu();
                break;
            case R.id.menu_clear_all_profile:
                DbUtils.removeProfile(getActivity(), ((ProfileCursorAdapter) mListView.getAdapter()).getAllProfiles());
                ((ProfileCursorAdapter) mListView.getAdapter()).resetRemoveAll();
                break;
            case R.id.refresh_menu_item:
                DbUtils.updateActiveStatus(getActivity(), ((ProfileCursorAdapter) mListView.getAdapter()).getDirtyProfiles());
                ((ProfileCursorAdapter) mListView.getAdapter()).resetDirtySet();
                createWidgetButtonList(((ProfileCursorAdapter) mListView.getAdapter()));
                break;
            case R.id.menu_setting:
                if (getActivity() != null) {
                    getActivity().startActivity(new Intent(getActivity(), SettingsActivity.class));
                }
                break;
            default:
                break;
        }

        if (mListView.getChildCount() <= 0) {
            mTextView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {ProfilesProvider.Profile._ID, ProfilesProvider.Profile._NAME, ProfilesProvider.Profile._ACTIVE};
        return new CursorLoader(getActivity(), ProfilesProvider.CONTENT_URI_PROFILE, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() == 0) {
            mTextView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        } else {
            mListView.setVisibility(View.VISIBLE);
            mTextView.setVisibility(View.GONE);
            cursor.moveToFirst();
            ProfileCursorAdapter adapter = new ProfileCursorAdapter(getActivity(), cursor);
            adapter.setIFinishLoad(this);
            adapter.setProfileCheckdChanged(this);
            mListView.setAdapter(adapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void createWidgetButtonList(ProfileCursorAdapter adapter) {
        Set<ProfileObject> set = adapter.getActiveList();
        HashSet<ProfileObject> setOfProfiles = null;
        if (set == null) {
            return;
        }

        if (set instanceof HashSet) {
            setOfProfiles = (HashSet<ProfileObject>) set;
        }

        Intent intent = new Intent(getString(R.string.widget_broadcast_provider));
        intent.putExtra("list_of_active_profiles", setOfProfiles);

        if (getActivity() != null) {
            getActivity().sendBroadcast(intent);
        }
    }

    // this method is not use now
    public void createButtonList(ProfileCursorAdapter adapter) {
        Set<ProfileObject> set = adapter.getActiveList();
        if (set == null || set.isEmpty()) {
            mButtonLayout.removeAllViews();
            return;
        }

        if (mSetProfiles == null || !mSetProfiles.equals(set)) {
            mSetProfiles = set;
        } else {
            return;
        }

        for (ProfileObject object : set) {
            final ProfileObject profileObject = object;
            Button profileButton = new Button(getActivity());
            profileButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
            profileButton.setText(object.getProfileName());
            profileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Cursor cursor = DbUtils.getProfileDetailFromProfileId(getActivity(), profileObject.getIndex());

                    List<ProfileDetailObject> profileDetails = DbUtils.createProfileDetailObjectFromCursor(getActivity(), MainActivity.getProfileSupportObject(getActivity()), cursor);
                    for (ProfileDetailObject profileDetailObject : profileDetails) {
                        if (profileDetailObject.getIsIsActive()) {
                            FunctionalUtils.callFunctionWithName(getActivity(), profileDetailObject);
                        }
                    }
                }
            });
            mButtonLayout.addView(profileButton);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null) {
            getActivity().getLoaderManager().destroyLoader(1);
        }
    }

    @Override
    public void onFinishLoad(ProfileCursorAdapter adapter) {
        DbUtils.updateActiveStatus(getActivity(), ((ProfileCursorAdapter) mListView.getAdapter()).getDirtyProfiles());
        ((ProfileCursorAdapter) mListView.getAdapter()).resetDirtySet();
        createWidgetButtonList(((ProfileCursorAdapter) mListView.getAdapter()));
    }

    @Override
    public void onProfileCheckChanged(boolean isChecked, Set<Long> deleteIds) {
        // if any of the row checked, display the delete menu
        mShowDeleteMenu = isChecked;
        mDeleteIds = deleteIds;
        if (getActivity() != null) {
            getActivity().invalidateOptionsMenu();
        }
    }
}
