/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.HashMap;

/**
 * @author Lam Tran on 3/2/14.
 */
public class ProfilesProvider extends ContentProvider {
    public static final String AUTHORITY = "lam.tran.vn.dailytaskautomator.provider";
    /**
     * Table specific
     */
    public static final String URL = "content://" + AUTHORITY + "/profile";
    public static final Uri CONTENT_URI_PROFILE = Uri.parse(URL);
    public static final String URL_PROFILE_DETAIL = "content://" + AUTHORITY + "/profile_detail";
    public static final Uri CONTENT_URI_PROFILE_DETAIL = Uri.parse(URL_PROFILE_DETAIL);
    static final String DATABASE_NAME = "DailyTaskAutomator";
    static final int DATABASE_VERSION = 1;
    static final int PROFILE = 1;
    static final int PROFILE_ID = 2;
    static final int PROFILE_DETAIL = 3;
    static final int PROFILE_DETAIL_ID = 4;
    static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "profile", PROFILE);
        uriMatcher.addURI(AUTHORITY, "profile/#", PROFILE_ID);
        uriMatcher.addURI(AUTHORITY, "profile_detail", PROFILE_DETAIL);
        uriMatcher.addURI(AUTHORITY, "profile_detail/#", PROFILE_DETAIL_ID);
    }
    static final String PROFILE_TABLE_NAME = "profiles";
    static final String CREATE_DB_TABLE =
            " CREATE TABLE " + PROFILE_TABLE_NAME +
                    " (" + Profile._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Profile._NAME + " TEXT NOT NULL, " +
                    Profile._ACTIVE + " INTEGER NOT NULL);";
    static final String PROFILE_DETAIL_TABLE_NAME = "profile_detail";
    static final String CREATE_PROFILE_DETAIL_TABLE =
            " CREATE TABLE " + PROFILE_DETAIL_TABLE_NAME +
                    " (" + ProfileDetail._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ProfileDetail._PROFILE_ID + " INTEGER NOT NULL, " +
                    ProfileDetail._FUNCTION_ITEM_ID + " INTEGER NOT NULL, " +
                    ProfileDetail._OPTION_SELECTED + " INTEGER NOT NULL, " +
                    ProfileDetail._ACTIVE + " INTEGER NOT NULL);";
    private static HashMap<String, String> PROFILES_PROJECTION_MAP;
    private static HashMap<String, String> PROFILE_DETAIL_PROJECTION_MAP;
    /**
     * DB constant
     */
    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        Context context = getContext();
        DatabaseHelper dbHelper = new DatabaseHelper(context);

        db = dbHelper.getWritableDatabase();
        return (db != null);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)) {
            case PROFILE:
                qb.setTables(PROFILE_TABLE_NAME);
                qb.setProjectionMap(PROFILES_PROJECTION_MAP);
                break;
            case PROFILE_ID:
                qb.setTables(PROFILE_TABLE_NAME);
                qb.appendWhere(Profile._ID + "=" + uri.getPathSegments().get(1));
                break;
            case PROFILE_DETAIL:
                qb.setTables(PROFILE_DETAIL_TABLE_NAME);
                qb.setProjectionMap(PROFILE_DETAIL_PROJECTION_MAP);
                break;
            case PROFILE_DETAIL_ID:
                qb.setTables(PROFILE_DETAIL_TABLE_NAME);
                qb.appendWhere(ProfileDetail._ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder == "") {

            sortOrder = "_id";
        }
        Cursor c = qb.query(db, projection, selection, selectionArgs,
                null, null, sortOrder);

        /**
         * register to watch a content URI for changes
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {

            case PROFILE:
                return "vnd.android.cursor.dir/vnd.lam.tran.vn.dailytaskautomator.profiles";

            case PROFILE_ID:
                return "vnd.android.cursor.item/vnd.lam.tran.vn.dailytaskautomator.profiles";

            case PROFILE_DETAIL:
                return "vnd.android.cursor.dir/vnd.lam.tran.vn.dailytaskautomator.profile_detail";

            case PROFILE_DETAIL_ID:
                return "vnd.android.cursor.item/vnd.lam.tran.vn.dailytaskautomator.profile_detail";

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID;
        Uri _uri;
        switch (uriMatcher.match(uri)) {
            case PROFILE:
            case PROFILE_ID:
                rowID = db.insert(PROFILE_TABLE_NAME, "", values);

                /**
                 * If record is added successfully
                 */
                if (rowID > 0) {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_PROFILE, rowID);
                    getContext().getContentResolver().notifyChange(_uri, null);
                    return _uri;
                }
                throw new SQLException("Failed to add a record into " + uri);
            case PROFILE_DETAIL:
            case PROFILE_DETAIL_ID:
                rowID = db.insert(PROFILE_DETAIL_TABLE_NAME, "", values);

                /**
                 * If record is added successfully
                 */
                if (rowID > 0) {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_PROFILE_DETAIL, rowID);
                    getContext().getContentResolver().notifyChange(_uri, null);
                    return _uri;
                }
                throw new SQLException("Failed to add a record into " + uri);
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        String id;

        switch (uriMatcher.match(uri)) {
            case PROFILE:
                count = db.delete(PROFILE_TABLE_NAME, selection, selectionArgs);
                break;
            case PROFILE_ID:
                id = uri.getPathSegments().get(1);
                count = db.delete(PROFILE_TABLE_NAME, ProfileDetail._ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            case PROFILE_DETAIL:
                count = db.delete(PROFILE_DETAIL_TABLE_NAME, selection, selectionArgs);
                break;
            case PROFILE_DETAIL_ID:
                id = uri.getPathSegments().get(1);
                count = db.delete(PROFILE_DETAIL_TABLE_NAME, ProfileDetail._ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;

        switch (uriMatcher.match(uri)) {
            case PROFILE:
                count = db.update(PROFILE_TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            case PROFILE_ID:
                count = db.update(PROFILE_TABLE_NAME, values, Profile._ID +
                        " = " + uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            case PROFILE_DETAIL:
                count = db.update(PROFILE_DETAIL_TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            case PROFILE_DETAIL_ID:
                count = db.update(PROFILE_DETAIL_TABLE_NAME, values, ProfileDetail._ID +
                        " = " + uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public static class Profile {
        public static final String _ID = "_id";
        public static final String _NAME = "_name";
        public static final String _ACTIVE = "_active";
    }

    public static class ProfileDetail {
        public static final String _ID = "_id";
        public static final String _PROFILE_ID = "_profile_id";
        public static final String _FUNCTION_ITEM_ID = "_function_item_id";
        public static final String _ACTIVE = "_active";
        public static final String _OPTION_SELECTED = "_option_selected";
    }

    /**
     * Helper class that actually creates and manages
     * the provider's underlying data repository.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
            db.execSQL(CREATE_PROFILE_DETAIL_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + PROFILE_TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + PROFILE_DETAIL_TABLE_NAME);
            onCreate(db);
        }
    }
}
