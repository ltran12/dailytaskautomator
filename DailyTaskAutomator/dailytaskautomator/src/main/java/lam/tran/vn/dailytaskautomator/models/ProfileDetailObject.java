/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.models;

import android.content.Context;

import java.util.List;

import lam.tran.vn.dailytaskautomator.interfaces.IProfiles;
import lam.tran.vn.dailytaskautomator.utils.Utils;

/**
 * Object contains all the info needed for a profile detail.
 *
 * @author Lam Tran on 3/1/14.
 */
public class ProfileDetailObject implements IProfiles {
    private int mFunctionTypeId;
    private int mIconId; // icon of the object
    private List<String> mOptions; // list of available option for the object
    private boolean mIsActive;
    private String mName;
    private int mOptionIndex; // current selected option for the object
    private boolean mIsDirty;
    private int mDbId;
    private String mMethodCallBack;

    public ProfileDetailObject(Context context, ProfileSupportObject.ProfileSupportType object) {
        mFunctionTypeId = object.id;
        mName = object.name;
        mIconId = Utils.getResourceId(context, object.icon_id, "drawable", context.getPackageName());
        mOptions = object.options;
        mMethodCallBack = object.func;
        mIsActive = false;
        mOptionIndex = 0;
        mDbId = -1;
    }

    public void setMethodCallBack(String methodCallBack) {
        mMethodCallBack = methodCallBack;
    }

    public String getMethodCallback() {
        return mMethodCallBack;
    }

    public void setDirty() {
        mIsDirty = true;
    }

    public boolean getIsDirty() {
        return mIsDirty;
    }

    public int getCurrentId() {
        return mFunctionTypeId;
    }

    public int getDbId() {
        return mDbId;
    }

    public void setDbId(int dbId) {
        mDbId = dbId;
    }

    public boolean getIsIsActive() {

        return mIsActive;
    }

    public void setIsActive(boolean issActive) {
        mIsActive = issActive;
    }

    public String getFunctionName() {
        return mName;
    }

    public void setFunctionName(String functionName) {
        mName = functionName;
    }

    public int getIconId() {
        return mIconId;
    }

    public void setIconId(int iconId) {
        mIconId = iconId;
    }

    public List<String> getOptions() {
        return mOptions;
    }

    public void setOptions(List<String> options) {
        mOptions = options;
    }

    public int getOptionIndex() {
        return mOptionIndex;
    }

    public void setOptionIndex(int optionIndex) {
        mOptionIndex = optionIndex;
    }
}
