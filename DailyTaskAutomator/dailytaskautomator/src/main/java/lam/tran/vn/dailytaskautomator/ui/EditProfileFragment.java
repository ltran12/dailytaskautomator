/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import lam.tran.vn.dailytaskautomator.R;
import lam.tran.vn.dailytaskautomator.adapters.ProfileListAdapter;
import lam.tran.vn.dailytaskautomator.interfaces.IFragmentSwap;
import lam.tran.vn.dailytaskautomator.models.ProfileDetailObject;
import lam.tran.vn.dailytaskautomator.providers.ProfilesProvider;
import lam.tran.vn.dailytaskautomator.utils.DbUtils;

/**
 * @author Lam Tran on 3/1/14.
 *         <p/>
 *         Create a new profile with different functionality choices
 */
public class EditProfileFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private IFragmentSwap mActivity;
    private EditText mEditText;
    private long mProfileId;
    private ListView mListView;
    private Button mSaveBtn;
    private ProfileListAdapter mAdapter;
    private String mProfileName;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof IFragmentSwap)) {
            throw new ClassCastException("Not IFragmentSwap");
        }
        mActivity = (IFragmentSwap) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mProfileId = mActivity.getCurrentIndex();
        mProfileName = mActivity.getProfileName();

        final View view = inflater.inflate(R.layout.create_profile, container, false);

        if (view == null || getActivity() == null) {
            return null;
        }

        mEditText = (EditText) view.findViewById(R.id.new_profile_name);
        mListView = (ListView) view.findViewById(R.id.create_profile_listview);
        mSaveBtn = (Button) view.findViewById(R.id.new_profile_save);

        mEditText.setText(mProfileName == null ? "" : mProfileName);
        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // validate profile name
                if (mEditText.getText() != null && mEditText.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.invalid_profile_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mAdapter != null) {
                    //get the list of all profile details and write it into db
                    List<ProfileDetailObject> list = mAdapter.getList();
                    insertProfileDetail(list);
                    Toast.makeText(getActivity(), getString(R.string.profile_save), Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.fail_to_save_profile), Toast.LENGTH_SHORT).show();
                }
            }
        });

        getActivity().getLoaderManager().initLoader(2, null, this);

        return view;
    }

    private void insertProfileDetail(List<ProfileDetailObject> list) {
        boolean isNewProfile = false;
        String profileName = mEditText.getText().toString();

        // Create a new profile case
        if (mProfileId < 0) {
            // create a new profile
            Uri profileUri = DbUtils.insertNewProfile(getActivity(), profileName);
            if (profileUri != null) {
                mProfileId = ContentUris.parseId(profileUri);
            }
            isNewProfile = true;
        } else if (mProfileName == null || !mProfileName.equals(profileName)) {
            // update existed profile
            DbUtils.updateProfile(getActivity(), mEditText.getText().toString(), mProfileId, 1);
        }

        for (ProfileDetailObject object : list) {
            if (!object.getIsDirty()) {
                // if it not dirty => nothing change, no need to update or create new row in db
                continue;
            }

            // else insert or update row
            int dbId = object.getDbId();

            if (isNewProfile || dbId < 0) {
                // create a new profile detail row
                DbUtils.insertNewProfileDetail(getActivity(), mProfileId, object);
            } else {
                // update to db row base on dbId as the id of profile detail row
                DbUtils.updateProfileDetail(getActivity(), dbId, mProfileId, object);
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mProfileId < 0) {
            mAdapter = new ProfileListAdapter(getActivity(), DbUtils.createProfileDetailObjectFromCursor(getActivity(), MainActivity.getProfileSupportObject(getActivity()), null));
            mListView.setAdapter(mAdapter);
            return null;
        }

        String[] projection = {ProfilesProvider.Profile._ID, ProfilesProvider.ProfileDetail._FUNCTION_ITEM_ID, ProfilesProvider.Profile._ACTIVE, ProfilesProvider.ProfileDetail._OPTION_SELECTED, ProfilesProvider.ProfileDetail._PROFILE_ID};
        String selection = ProfilesProvider.ProfileDetail._PROFILE_ID + " = ?";
        String[] selectionArgs = {Long.toString(mProfileId)};
        return new CursorLoader(getActivity(), ProfilesProvider.CONTENT_URI_PROFILE_DETAIL, projection, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (getActivity() == null) {
            return;
        }

        mAdapter = new ProfileListAdapter(getActivity(), DbUtils.createProfileDetailObjectFromCursor(getActivity(), MainActivity.getProfileSupportObject(getActivity()), cursor));
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.setCurrentIndex(-1);
        getActivity().getLoaderManager().destroyLoader(2);
    }
}
