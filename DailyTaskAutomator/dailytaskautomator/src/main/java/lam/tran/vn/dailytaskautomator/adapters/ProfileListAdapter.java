/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import lam.tran.vn.dailytaskautomator.R;
import lam.tran.vn.dailytaskautomator.interfaces.IProfiles;
import lam.tran.vn.dailytaskautomator.models.ProfileDetailObject;

/**
 * @author Lam Tran on 3/1/14.
 */
public class ProfileListAdapter extends ArrayAdapter<ProfileDetailObject> {
    private final Context mContext;
    private List<ProfileDetailObject> mList;

    public ProfileListAdapter(Context context, List<ProfileDetailObject> list) {
        super(context, R.layout.profile_row);
        mContext = context;
        mList = list;
    }

    public List<ProfileDetailObject> getList() {
        return mList;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    /**
     * Whatever the row that the user change, it will be mark as dirty and it need to be updated to db
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mList == null) {
            return convertView;
        }

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.profile_row, parent, false);
        }
        final ViewHolder viewHolder = new ViewHolder(convertView);
        IProfiles profile = mList.get(position);

        final ProfileDetailObject object = (ProfileDetailObject) profile;

        viewHolder.mSwitch.setVisibility(View.GONE);
        viewHolder.mTextView.setText(object.getFunctionName());
        viewHolder.mImageView.setImageDrawable(mContext.getResources().getDrawable(object.getIconId()));

        viewHolder.mCheckBox.setChecked(object.getIsIsActive());
        setRowActionIfCheck(object, viewHolder);
        viewHolder.mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRowActionIfCheck(object, viewHolder);
                object.setDirty();
            }
        });

        if (object.getOptions().size() == 0) {
            viewHolder.mSpinner.setVisibility(View.GONE);
        } else {
            viewHolder.mSpinner.setVisibility(View.VISIBLE);
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, object.getOptions());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            viewHolder.mSpinner.setAdapter(adapter);
            viewHolder.mSpinner.setSelection(object.getOptionIndex());
            viewHolder.mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    object.setOptionIndex(position);
                    object.setDirty();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        return convertView;
    }

    /**
     * Change the row background color depends on the status of the checkbox
     */
    private void setRowActionIfCheck(ProfileDetailObject object, ViewHolder viewHolder) {
        if (!viewHolder.mCheckBox.isChecked()) {
            viewHolder.mLayout.setBackgroundResource(R.drawable.list_item_dither_state);
        } else {
            viewHolder.mLayout.setBackgroundResource(R.drawable.list_item_normal_state);
        }
        object.setIsActive(viewHolder.mCheckBox.isChecked());
    }

    private final class ViewHolder {
        public TextView mTextView;
        public CheckBox mCheckBox;
        public ImageView mImageView;
        public LinearLayout mLayout;
        public Spinner mSpinner;
        public Switch mSwitch;

        public ViewHolder(View viewRoot) {
            if (viewRoot == null) {
                return;
            }

            mTextView = (TextView) viewRoot.findViewById(R.id.profile_row_text);
            mCheckBox = (CheckBox) viewRoot.findViewById(R.id.profile_row_check_box);
            mImageView = (ImageView) viewRoot.findViewById(R.id.profile_row_icon);
            mLayout = (LinearLayout) viewRoot.findViewById(R.id.profile_row_layout);
            mSpinner = (Spinner) viewRoot.findViewById(R.id.profile_row_spinner);
            mSwitch = (Switch) viewRoot.findViewById(R.id.profile_row_switch);
        }
    }
}
