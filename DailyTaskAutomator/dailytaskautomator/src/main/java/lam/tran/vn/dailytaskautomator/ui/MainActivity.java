/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.ui;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import java.util.EmptyStackException;
import java.util.Stack;

import lam.tran.vn.dailytaskautomator.R;
import lam.tran.vn.dailytaskautomator.interfaces.IFragmentSwap;
import lam.tran.vn.dailytaskautomator.models.ProfileSupportObject;
import lam.tran.vn.dailytaskautomator.utils.Utils;

public class MainActivity extends Activity implements IFragmentSwap {
    public static ProfileSupportObject mProfileSupportObject;
    private Stack<Class> mFragmentStatck;
    private Fragment mCurrentFragment;
    private int mCurrentIndex;
    private String mProfileName;

    public static ProfileSupportObject getProfileSupportObject(Context context) {
        if (mProfileSupportObject == null) {
            return mProfileSupportObject = Utils.getProfileSupport(context);
        } else {
            return mProfileSupportObject;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (mFragmentStatck == null) {
            mFragmentStatck = new Stack<>();
        }
        if (mCurrentFragment == null) {
            mCurrentFragment = Utils.switchToFrag(this, R.id.fragment_container, ProfileFragment.class);
            mFragmentStatck.push(ProfileFragment.class);
        }
    }

    @Override
    public void onFragmentPop() {
        if (mFragmentStatck == null) {
            mFragmentStatck = new Stack<>();
        }
        Class fragmentClass = null;
        try {
            fragmentClass = mFragmentStatck.peek();
            if (fragmentClass != ProfileFragment.class) {
                mFragmentStatck.pop();
            } else {
                finish();
            }

            fragmentClass = mFragmentStatck.peek();
        } catch (EmptyStackException e) {
            // if stack is empty and user hit back again, finish activity
            finish();
        }

        mCurrentFragment = Utils.switchToFrag(this, R.id.fragment_container, fragmentClass);
    }

    @Override
    public void onFragmentPush(Class fragmentClass) {
        if (mFragmentStatck == null) {
            mFragmentStatck = new Stack<>();
        }

        mCurrentFragment = Utils.switchToFrag(this, R.id.fragment_container, fragmentClass);
        mFragmentStatck.push(fragmentClass);
    }

    @Override
    public String getProfileName() {
        return mProfileName;
    }

    @Override
    public void setProfileName(String name) {
        mProfileName = name;
    }

    @Override
    public long getCurrentIndex() {
        return mCurrentIndex;
    }

    @Override
    public void setCurrentIndex(int index) {
        mCurrentIndex = index;
    }

    @Override
    public void onBackPressed() {
        if (mFragmentStatck != null) {
            onFragmentPop();
        }
    }
}
