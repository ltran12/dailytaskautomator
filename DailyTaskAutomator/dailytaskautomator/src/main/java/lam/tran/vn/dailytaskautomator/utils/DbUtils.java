/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lam.tran.vn.dailytaskautomator.models.ProfileDetailObject;
import lam.tran.vn.dailytaskautomator.models.ProfileObject;
import lam.tran.vn.dailytaskautomator.models.ProfileSupportObject;
import lam.tran.vn.dailytaskautomator.providers.ProfilesProvider;

/**
 * This class contains all of the utilities function to help read/write to db
 *
 * @author Lam Tran on 3/8/14.
 */
public class DbUtils {
    /**
     * Query and get all the profile details from profile id.
     *
     * @param context   Context of the app.
     * @param profileId current id of the profile.
     * @return Cursor point to the rows.
     */
    public static Cursor getProfileDetailFromProfileId(Context context, long profileId) {
        String[] projection = {ProfilesProvider.Profile._ID, ProfilesProvider.ProfileDetail._FUNCTION_ITEM_ID, ProfilesProvider.Profile._ACTIVE, ProfilesProvider.ProfileDetail._OPTION_SELECTED, ProfilesProvider.ProfileDetail._PROFILE_ID};
        String selection = ProfilesProvider.ProfileDetail._PROFILE_ID + " = ?";
        String[] selectionArgs = {Long.toString(profileId)};
        return context.getContentResolver().query(ProfilesProvider.CONTENT_URI_PROFILE_DETAIL, projection, selection, selectionArgs, null);
    }

    /**
     * Insert profile
     *
     * @param context     Context of the app.
     * @param profileName current id of the profile.
     * @return Uri from the new insert row.
     */
    public static Uri insertNewProfile(Context context, String profileName) {
        ContentValues values = new ContentValues();
        // create a new profile first
        values.put(ProfilesProvider.Profile._NAME, profileName);
        values.put(ProfilesProvider.Profile._ACTIVE, 1);
        return context.getContentResolver().insert(ProfilesProvider.CONTENT_URI_PROFILE, values);
    }

    /**
     * update profile
     *
     * @param context     Context of the app.
     * @param profileName current id of the profile.
     * @param index       Index of the existed row.
     * @return True if update is success.
     */
    public static boolean updateProfile(Context context, String profileName, long index, int isActive) {
        ContentValues values = new ContentValues();
        values.put(ProfilesProvider.Profile._NAME, (profileName));
        values.put(ProfilesProvider.Profile._ACTIVE, isActive);

        String selection = ProfilesProvider.Profile._ID + " = ?";
        String[] selectionArgs = {Long.toString(index)};
        int updates = context.getContentResolver().update(ProfilesProvider.CONTENT_URI_PROFILE, values, selection, selectionArgs);
        return updates != 0;
    }

    /**
     * Update active status base on the set
     *
     * @param context  Context of the app.
     * @param profiles Set of active change profile.
     */
    public static void updateActiveStatus(Context context, Set<ProfileObject> profiles) {
        for (ProfileObject profile : profiles) {
            updateProfile(context, profile.getProfileName(), profile.getIndex(), profile.isIsActive() ? 1 : 0);
        }
    }

    /**
     * Insert profile detail
     *
     * @param context   Context of the app.
     * @param profileId current id of the profile.
     * @param object    The ProfileDetailObject.
     */
    public static void insertNewProfileDetail(Context context, long profileId, ProfileDetailObject object) {
        context.getContentResolver().insert(ProfilesProvider.CONTENT_URI_PROFILE_DETAIL, buildProfileDetailFromObject(object, profileId));
    }

    /**
     * Update profile detail
     *
     * @param context   Context of the app.
     * @param index     Index of the existed row.
     * @param profileId current id of the profile.
     * @param object    The ProfileDetailObject.
     */
    public static void updateProfileDetail(Context context, long index, long profileId, ProfileDetailObject object) {
        String selection = ProfilesProvider.ProfileDetail._ID + " = ?";
        String[] selectionArgs = {Long.toString(index)};
        context.getContentResolver().update(ProfilesProvider.CONTENT_URI_PROFILE_DETAIL, buildProfileDetailFromObject(object, profileId), selection, selectionArgs);
    }

    /**
     * Build profile detail.
     *
     * @param object    The ProfileDetailObject.
     * @param profileId current id of the profile.
     * @return Values that build from the objects.
     */
    private static ContentValues buildProfileDetailFromObject(ProfileDetailObject object, long profileId) {
        ContentValues values = new ContentValues();
        values.put(ProfilesProvider.ProfileDetail._PROFILE_ID, profileId);
        values.put(ProfilesProvider.ProfileDetail._ACTIVE, object.getIsIsActive());
        values.put(ProfilesProvider.ProfileDetail._FUNCTION_ITEM_ID, object.getCurrentId());
        values.put(ProfilesProvider.ProfileDetail._OPTION_SELECTED, object.getOptionIndex());
        return values;
    }

    /**
     * Delete profile and all profile details has the profile id
     *
     * @param context    Context of the app.
     * @param profileIds Set of profileId.
     */
    public static void removeProfile(Context context, Set<Long> profileIds) {
        for (long profileId : profileIds) {
            // remove row in profile table
            String profileSelection = ProfilesProvider.Profile._ID + " = ?";
            String[] profileSelectionArg = {Long.toString(profileId)};
            context.getContentResolver().delete(ProfilesProvider.CONTENT_URI_PROFILE, profileSelection, profileSelectionArg);

            // remove rows in profile detail table
            String detailSelection = ProfilesProvider.ProfileDetail._PROFILE_ID + " = ?";
            String[] detailSelectionArg = {Long.toString(profileId)};
            context.getContentResolver().delete(ProfilesProvider.CONTENT_URI_PROFILE_DETAIL, detailSelection, detailSelectionArg);
        }
    }

    /**
     * Take list and cursor and mix them together to the final list of ProfileDetailObject to display to the screen
     *
     * @param context application context.
     * @param list    List of profile details that read from json
     * @param cursor  cursor point to the profile detail row from db
     * @return list of ProfileDetailObject
     */
    public static List<ProfileDetailObject> createProfileDetailObjectFromCursor(Context context, ProfileSupportObject list, Cursor cursor) {
        if (list == null) {
            return null;
        }

        List<ProfileDetailObject> finalList = new ArrayList<>();
        Map<Integer, ProfileDetailObject> profileDetailMap = new HashMap<>();
        // create a base map from asset
        for (ProfileSupportObject.ProfileSupportType object : list.profile_support_list) {
            profileDetailMap.put(object.id, new ProfileDetailObject(context, object));
        }

        if (cursor != null) {
            cursor.moveToFirst();
            do {
                //typeId is also be map id
                int typeId = cursor.getInt(cursor.getColumnIndex(ProfilesProvider.ProfileDetail._FUNCTION_ITEM_ID));
                int dbId = cursor.getInt(cursor.getColumnIndex(ProfilesProvider.ProfileDetail._ID));
                int isActive = cursor.getInt(cursor.getColumnIndex(ProfilesProvider.ProfileDetail._ACTIVE));
                int optionSelected = cursor.getInt(cursor.getColumnIndex(ProfilesProvider.ProfileDetail._OPTION_SELECTED));

                ProfileDetailObject profileDetailObject = profileDetailMap.get(typeId);
                profileDetailObject.setDbId(dbId);
                profileDetailObject.setIsActive(isActive != 0);
                profileDetailObject.setOptionIndex(optionSelected);
            } while (cursor.moveToNext());
        }

        for (Map.Entry<Integer, ProfileDetailObject> entry : profileDetailMap.entrySet()) {
            finalList.add(entry.getValue());
        }

        return finalList;
    }
}
