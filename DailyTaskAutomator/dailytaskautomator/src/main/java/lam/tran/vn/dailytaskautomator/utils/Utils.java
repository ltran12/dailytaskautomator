/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import lam.tran.vn.dailytaskautomator.models.ProfileSupportObject;

/**
 * All general utils
 *
 * @author Lam Tran on 3/2/14.
 */
public class Utils {
    /**
     * Read the json file from asset and create profile support object which is a list of object details
     *
     * @param context application context.
     * @return ProfileSupportObject
     */
    public static ProfileSupportObject getProfileSupport(Context context) {
        Gson gson = new Gson();
        String content = "";
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("SupportFunctions.json")));

            String line;
            while ((line = reader.readLine()) != null) {
                content += line;
            }

            reader.close();
        } catch (IOException e) {
            Log.e("JsonUtils", "IOException" + e.getMessage());
        }
        return gson.fromJson(content, ProfileSupportObject.class);
    }

    /**
     * Create a new fragment form fragment class and replace the old one
     *
     * @param activity  application context
     * @param container container resource
     * @param clazz     fragment class
     * @return new fragment after switch
     */
    public static Fragment switchToFrag(Activity activity, int container, Class clazz) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) clazz.newInstance();
            FragmentManager fragmentManager = activity.getFragmentManager();
            fragmentManager.beginTransaction().replace(container, fragment, clazz.toString()).commit();

        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return fragment;
    }

    /**
     * Get resource id from its string name
     *
     * @param context      application context.
     * @param variableName variable name in resource.
     * @param resourcename resource type (e.g: string, drawable, etc...).
     * @param packageName  package name
     * @return resource id.
     */
    public static int getResourceId(Context context, String variableName, String resourcename, String packageName) {
        if (context == null) {
            return -1;
        }
        try {
            return context.getResources().getIdentifier(variableName, resourcename, packageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    /* public static boolean writeProfilesList(Context context, List<IProfiles> profileObjects) {
         FileOutputStream writer;
         try {
             writer = context.openFileOutput("Profiles.json", Context.MODE_PRIVATE);
             Gson gson = new Gson();
             writer.write(gson.toJson(profileObjects).getBytes());
             writer.close();
             return true;
         } catch (IOException e) {
             e.printStackTrace();
             return false;
         }
     }

     public static List<IProfiles> readProfilesList(Context context) {
         InputStream reader;
         String content = "";
         try {
             reader = context.openFileInput("Profiles.json");

             if (reader != null) {
                 InputStreamReader inputStreamReader = new InputStreamReader(reader);
                 BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                 String receivedString = "";

                 while ((receivedString = bufferedReader.readLine()) != null) {
                     content += receivedString;
                 }

                 inputStreamReader.close();
                 bufferedReader.close();
             }

             Gson gson = new Gson();
             List<LinkedTreeMap> listOfMap = gson.fromJson(content, List.class);
             List<IProfiles> list = new ArrayList<>();
             for (LinkedTreeMap map : listOfMap) {
                 list.add(new ProfileObject((int) Double.parseDouble(map.get("mIndex").toString()), map.get("mProfileName").toString(), Boolean.getBoolean(map.get("mIsActive").toString())));
             }
             return list;
         } catch (IOException e) {
             e.printStackTrace();
             return null;
         }
     }

     public static boolean writeProfilesFunctionList(Context context, int index, Map<Integer, Pair<Boolean, Integer>> activeMap) {
         FileOutputStream writer;
         try {
             writer = context.openFileOutput("ProfileFunctions" + index + ".json", Context.MODE_PRIVATE);
             Gson gson = new Gson();
             writer.write(gson.toJson(activeMap).getBytes());
             writer.flush();
             writer.close();
             return true;
         } catch (IOException e) {
             e.printStackTrace();
             return false;
         }
     }

     public static Map<Integer, Pair<Boolean, Integer>> readProfilesFunctionList(Context context, int index) {
         InputStream reader;
         String content = "";
         try {
             reader = context.openFileInput("ProfileFunctions-" + index + ".json");

             if (reader != null) {
                 InputStreamReader inputStreamReader = new InputStreamReader(reader);
                 BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                 String receivedString = "";

                 while ((receivedString = bufferedReader.readLine()) != null) {
                     content += receivedString;
                 }

                 inputStreamReader.close();
                 bufferedReader.close();
             }

             Gson gson = new Gson();
             Map<String, LinkedTreeMap> mapOfMap = gson.fromJson(content, Map.class);
             Map<Integer, Pair<Boolean, Integer>> map = new HashMap<>();

             Map linkedMap;
             Pair<Boolean, Integer> pair;

             for (int i = 0; i < 4; i++) {
                 linkedMap = mapOfMap.get(Integer.toString(i));
                 boolean bool = Boolean.getBoolean(linkedMap.get("first").toString());
                 int dou = (int) Double.parseDouble(linkedMap.get("second").toString());
                 pair = new Pair(bool, dou);
                 map.put(i, pair);
             }

             return map;
         } catch (IOException e) {
             e.printStackTrace();
             return null;
         }
     }
 */


}
