/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.interfaces;

/**
 * @author Lam Tran on 3/1/14.
 */
public interface IFragmentSwap {
    /**
     * Pop the last fragment and replace the current fragment
     */
    public void onFragmentPop();

    /**
     * Switch to new fragment and push the new one into the stack
     */
    public void onFragmentPush(Class fragmentClass);

    /**
     * Get bundle from other fragment
     */
    public long getCurrentIndex();

    /**
     * Set bundle to transfer to other framgent
     */
    public void setCurrentIndex(int index);

    public String getProfileName();

    public void setProfileName(String name);
}
