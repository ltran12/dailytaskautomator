/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import lam.tran.vn.dailytaskautomator.R;
import lam.tran.vn.dailytaskautomator.models.ProfileDetailObject;

/**
 * All the function calls in here
 *
 * @author Lam Tran on 3/1/14.
 */
public class FunctionalUtils {
    /**
     * Function to enable and disable wifi
     *
     * @param context  context - context of the app
     * @param isEnable 0 for false and other for true
     */
    public static void enableWifi(final Context context, final Integer isEnable) {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(isEnable == 0);
    }

    /**
     * Function to enable and disable wifi
     *
     * @param context  context - context of the app
     * @param isEnable 0 for false and other for true
     */
    public static void enableMobileNetwork(final Context context, final Integer isEnable) {
        final ConnectivityManager dataManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
            dataMtd.setAccessible(true);
            dataMtd.invoke(dataManager, isEnable == 0);

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            Toast.makeText(context, context.getString(R.string.crash_mobile_network), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Function to enable and disable bluetooth
     *
     * @param context  context - context of the app
     * @param isEnable 0 for false and other for true
     */
    public static void enableBluetooth(final Context context, final Integer isEnable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (isEnable == 0) {
            bluetoothAdapter.enable();
        } else {
            bluetoothAdapter.disable();
        }
    }

    /**
     * Function to set device ringtone
     *
     * @param context     context - context of the app
     * @param volumeState 0 - silence, 1 - vibration, 2 - normal
     */
    public static void turnVolumn(final Context context, final Integer volumeState) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        switch (volumeState) {
            case 0:
                audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                break;
            case 1:
                audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                break;
            case 2:
                audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                break;
            default:
                break;
        }
    }

    /**
     * Function to set device alarm ringtone
     *
     * @param context context - context of the app
     * @param index   volume set
     */
    public static void turnAlarmVolumn(final Context context, final Integer index) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_ALARM, (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM) * ((float) index / 5)), AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
    }

    /**
     * FUnction to end call
     *
     * @param context context - context of the app
     * @param index   not use in this case but need to make it to be consistent
     */
    public static void endCall(final Context context, final Integer index) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        Class clazz;
        try {
            clazz = Class.forName(telephonyManager.getClass().getName());
            Method method;
            method = clazz.getDeclaredMethod("getITelephony");
            method.setAccessible(true);
            ITelephony telephonyService;

            telephonyService = (ITelephony) method.invoke(telephonyManager);
            telephonyService.endCall();


        } catch (RemoteException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Call function by its name from profile detail object
     *
     * @param context             Current application context
     * @param profileDetailObject profile detail object
     */
    public static void callFunctionWithName(final Context context, ProfileDetailObject profileDetailObject) {
        try {
            Method method = FunctionalUtils.class.getDeclaredMethod(profileDetailObject.getMethodCallback(), Context.class, Integer.class);
            method.invoke(null, context, profileDetailObject.getOptionIndex());
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            Log.e("FunctionalUtils", "Fail to invoke method " + profileDetailObject.getMethodCallback());
        }
    }
}
