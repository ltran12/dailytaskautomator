/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.providers;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.RemoteViews;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lam.tran.vn.dailytaskautomator.R;
import lam.tran.vn.dailytaskautomator.models.ProfileDetailObject;
import lam.tran.vn.dailytaskautomator.models.ProfileObject;
import lam.tran.vn.dailytaskautomator.ui.MainActivity;
import lam.tran.vn.dailytaskautomator.utils.DbUtils;
import lam.tran.vn.dailytaskautomator.utils.FunctionalUtils;
import lam.tran.vn.dailytaskautomator.utils.Utils;

/**
 * @author Lam Tran on 3/12/14.
 */
public class DailyTaskProvider extends AppWidgetProvider {
    private Set mActiveSet;
    private boolean mForceUpdate;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        ProfileObject profileObject;
        if (mActiveSet == null && !mForceUpdate) {
            return;
        }

        mForceUpdate = false;
        for (int i = 0; i < appWidgetIds.length; i++) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.app_widget);

            int index = 0;
            int resourceId;

            Iterator<ProfileObject> profileObjectIterator = mActiveSet.iterator();
            for (; index < Math.min(5, mActiveSet.size()); ++index) {
                resourceId = Utils.getResourceId(context, "btn_widget_" + index, "id", context.getPackageName());
                remoteViews.setViewVisibility(resourceId, View.VISIBLE);
                profileObject = profileObjectIterator.next();
                remoteViews.setTextViewText(resourceId, profileObject.getProfileName());

                Intent intent = new Intent(context.getString(R.string.widget_action_click_action));
                intent.putExtra("profile_object", profileObject);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, index, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setOnClickPendingIntent(resourceId, pendingIntent);
            }

            for (; index < 5; ++index) {
                resourceId = Utils.getResourceId(context, "btn_widget_" + index, "id", context.getPackageName());
                remoteViews.setViewVisibility(resourceId, View.GONE);
            }
            appWidgetManager.updateAppWidget(appWidgetIds[i], remoteViews);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction() == null) {
            // if it's not the one we send out then do nothing
            return;
        }

        if (intent.getAction().equals(context.getString(R.string.widget_broadcast_provider))) {
            // receive widget broadcast action to update active set

            if (intent.getSerializableExtra("list_of_active_profiles") != null && intent.getSerializableExtra("list_of_active_profiles") instanceof Set) {
                Set activeSet = (Set) intent.getSerializableExtra("list_of_active_profiles");
                if (activeSet != null && activeSet.equals(mActiveSet)) {
                    // if the new data coming in is the same as the data already set, dont do anything
                    return;
                }

                mActiveSet = activeSet;
                if (mActiveSet != null && mActiveSet.size() <= 0) {
                    mForceUpdate = true;
                }
            }

            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), DailyTaskProvider.class.getName());
            int[] appWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(thisAppWidget);
            onUpdate(context, AppWidgetManager.getInstance(context), appWidgetIds);

        } else if (intent.getAction().equals(context.getString(R.string.widget_action_click_action))) {
            // receive button click action to do the action
            if (intent.getSerializableExtra("profile_object") != null) {
                doActionButtonClick(context, (ProfileObject) intent.getSerializableExtra("profile_object"));
            }
        }
    }

    /**
     * Handle button click action
     *
     * @param context       context that application is on
     * @param profileObject object of current profile
     */
    public void doActionButtonClick(final Context context, final ProfileObject profileObject) {
        Cursor cursor = DbUtils.getProfileDetailFromProfileId(context, profileObject.getIndex());

        List<ProfileDetailObject> profileDetails = DbUtils.createProfileDetailObjectFromCursor(context, MainActivity.getProfileSupportObject(context), cursor);
        for (ProfileDetailObject profileDetailObject : profileDetails) {
            if (profileDetailObject.getIsIsActive()) {

                FunctionalUtils.callFunctionWithName(context, profileDetailObject);
            }
        }
    }
}
