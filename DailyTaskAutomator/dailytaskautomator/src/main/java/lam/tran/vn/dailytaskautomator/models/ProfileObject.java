/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.models;

import java.io.Serializable;

import lam.tran.vn.dailytaskautomator.interfaces.IProfiles;

/**
 * @author Lam Tran  lam on 3/2/14.
 */
public class ProfileObject implements IProfiles, Serializable {
    private String mProfileName;
    private boolean mIsActive;
    private long mIndex;

    public ProfileObject(long index, String profileName, boolean isActive) {
        mIndex = index;
        mProfileName = profileName;
        mIsActive = isActive;
    }

    public long getIndex() {
        return mIndex;
    }

    public String getProfileName() {
        return mProfileName;
    }

    public void setProfileName(String profileName) {
        mProfileName = profileName;
    }

    public boolean isIsActive() {
        return mIsActive;
    }

    public void setIsActive(boolean isActive) {
        mIsActive = isActive;
    }

    @Override
    public int hashCode() {
        return (int) (mIndex ^ (mIndex >>> 32));
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof ProfileObject) {
            ProfileObject object = (ProfileObject) o;
            if (getIndex() == object.getIndex() && getProfileName().equals(object.getProfileName())) {
                return true;
            }
        }
        return false;
    }
}
