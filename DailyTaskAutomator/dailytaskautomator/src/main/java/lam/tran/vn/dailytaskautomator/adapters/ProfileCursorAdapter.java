/**
 Copyright 2014 Lam Tran

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package lam.tran.vn.dailytaskautomator.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

import lam.tran.vn.dailytaskautomator.R;
import lam.tran.vn.dailytaskautomator.models.ProfileObject;
import lam.tran.vn.dailytaskautomator.providers.ProfilesProvider;

/**
 * Build a list view base on cursor from db
 *
 * @author Lam Tran on 3/2/14.
 */
public class ProfileCursorAdapter extends CursorAdapter {
    private Set<ProfileObject> mActiveSet = new HashSet<>();
    private IFinishLoad mFInishLoad;
    private IProfileCheckChanged mProfileCheckedChanged;
    private Set<Long> mDeleteIds = new HashSet<>();
    private Set<Long> mAllIds = new HashSet<>();
    private Set<ProfileObject> mDirtySet = new HashSet<>();
    private boolean mChecked;

    public ProfileCursorAdapter(Context context, Cursor c) {
        super(context, c, true);
    }

    public void setIFinishLoad(IFinishLoad finishLoad) {
        mFInishLoad = finishLoad;
    }

    public void setProfileCheckdChanged(IProfileCheckChanged profileCheckedChanged) {
        mProfileCheckedChanged = profileCheckedChanged;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.profile_row, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder viewHolder = new ViewHolder(view);

        int isActive = cursor.getInt(cursor.getColumnIndex(ProfilesProvider.Profile._ACTIVE));
        final String name = cursor.getString(cursor.getColumnIndex(ProfilesProvider.Profile._NAME));
        final long id = cursor.getInt(cursor.getColumnIndex(ProfilesProvider.Profile._ID));

        mAllIds.add(id);
        viewHolder.mSpinner.setVisibility(View.GONE);
        viewHolder.mImageView.setVisibility(View.GONE);
        viewHolder.mTextView.setText(name);

        viewHolder.mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.mCheckBox.isChecked()) {
                    mDeleteIds.add(id);
                } else {
                    mDeleteIds.remove(id);
                }

                validateAndCallOnProfileCheckedChanged();
            }
        });


        viewHolder.mSwitch.setChecked(isActive != 0);
        viewHolder.mSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set the profile to be active or not
                ProfileObject profile = new ProfileObject(id, name, viewHolder.mSwitch.isChecked());
                if (viewHolder.mSwitch.isChecked()) {
                    mActiveSet.add(profile);
                } else {
                    mActiveSet.remove(profile);
                }

                mDirtySet.add(profile);

            }
        });

        view.setTag(R.string.profile_name, name);
        view.setTag(R.string.profile_index, id);
        if (isActive != 0) {
            mActiveSet.add(new ProfileObject(id, name, isActive != 0));
        }

        if (cursor.isLast()) {
            mFInishLoad.onFinishLoad(this);
            //mActiveSet.clear();
        }
    }

    public Set<Long> getAllProfiles() {
        return mAllIds;
    }

    public Set<ProfileObject> getDirtyProfiles() {
        return mDirtySet;
    }

    /**
     * Reset all the remove check variable
     */
    public void resetRemoveChecked() {
        mChecked = false;
        mDeleteIds.clear();
        mAllIds.removeAll(mDeleteIds);
        validateAndCallOnProfileCheckedChanged();
    }

    /**
     * Reset all the check variable
     */
    public void resetRemoveAll() {
        mActiveSet.clear();
        mChecked = false;
        mDeleteIds.clear();
        mAllIds.clear();
    }

    /**
     * reset the dirty set
     */
    public void resetDirtySet() {
        mDirtySet.clear();
    }

    private void validateAndCallOnProfileCheckedChanged() {
        if (!mChecked && mDeleteIds.size() > 0) {
            mChecked = true;
            mProfileCheckedChanged.onProfileCheckChanged(true, mDeleteIds);
        } else if (mChecked && mDeleteIds.size() == 0) {
            mChecked = false;
            mProfileCheckedChanged.onProfileCheckChanged(false, null);
        }
    }

    public Set<ProfileObject> getActiveList() {
        return mActiveSet;
    }

    public interface IFinishLoad {
        public void onFinishLoad(ProfileCursorAdapter adapter);
    }

    public interface IProfileCheckChanged {
        public void onProfileCheckChanged(boolean isChecked, Set<Long> deleteIds);
    }

    /**
     * View holder to hold all of the view in profile row
     */
    private final class ViewHolder {
        public TextView mTextView;
        public CheckBox mCheckBox;
        public ImageView mImageView;
        public LinearLayout mLayout;
        public Spinner mSpinner;
        public Switch mSwitch;

        public ViewHolder(View viewRoot) {
            if (viewRoot == null) {
                return;
            }

            mTextView = (TextView) viewRoot.findViewById(R.id.profile_row_text);
            mCheckBox = (CheckBox) viewRoot.findViewById(R.id.profile_row_check_box);
            mImageView = (ImageView) viewRoot.findViewById(R.id.profile_row_icon);
            mLayout = (LinearLayout) viewRoot.findViewById(R.id.profile_row_layout);
            mSpinner = (Spinner) viewRoot.findViewById(R.id.profile_row_spinner);
            mSwitch = (Switch) viewRoot.findViewById(R.id.profile_row_switch);
        }
    }
}
